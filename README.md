# Benchmarking Error Mitigation Techniques

This is a [final project](https://gitlab.com/qworld/qeducation/qcourse511-1/-/issues/21) for [QCourse511](https://gitlab.com/qworld/qeducation/qcourse511-1/) created by the team "Quantum Eraser".

## Members

Amir Ebrahimi, @amirebrahimi  
Manvi Gusain, @Manvi333  
Ranendu Adhikary, @ronjumath  
Saumya Prakash Sharma, @saumya.sharma1  
Tugba Özbilgin, @tozbilgin  

## Deliverables
[Read our technical report](BenchmarkingQEM.pdf) on benchmarking Zero-noise Extrapolation (ZNE), Probalistic Error Cancellation (PEC), and Clifford Data Regression (CDR) against different noise models. Additionally, Randomized Compiling (RC) is explored.

For details behind our execution of the project, you may [read our final report](FinalReport.md).

## Resources

The Jupyter notebooks in this main directory contain code that was used in the technical report. 

Additionally, there are supplementary notebooks we have used in the process of exploring the different QEM techniques in our report. These are located under:
- [ZNE](ZNE)
- [PEC](PEC)
- [CDR](CDR)
- [RC](RC)

## License

All of the content in this repository is licensed under [Apache License 2.0](LICENSE)