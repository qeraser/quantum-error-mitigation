```python

```

<!-- #region -->
# Midterm Report

## Introduction

Quantum computing is the encoding of information onto quantum bits (qubits) and processing that information using quantum algorithms. Quantum computers can exploit a Hilbert space whose dimension increases exponentially with the number of qubits. Mostly, quantum algorithms are developed with the assumption that qubits are perfect in that they obey quantum mechanical laws with perfect precision and are often referred to as logical qubits. Physical qubits, on the other hand, refer to the actual implementation of qubits in quantum computing devices. Unfortunately, these physical qubits suffer from imperfections like quantum noise and decoherence. Mitigation of errors is crucial to obtain reliable results. The inevitable accumulation of errors in near-term quantum devices represents a key obstacle in delivering practical quantum advantage. 

Quantum error correction (QEC) was offered as a solution to mitigate the effects of quantum noise and decoherence by spreading the information of one qubit on an entangled state over several qubits. However, QEC techniques have many drawbacks as they generally require a large overhead in terms of additional qubits on top of those required for the quantum computation. Current quantum computing devices have been able to demonstrate QEC only with a very small number of qubits. Therefore, another mitigation techniques called quantum error mitigation (QEM) has arisen. QEM is generally considered as a series of techniques that are more practical than QEC solutions as they offer low-overhead methods to more accurately and reliably estimate observable values than unmitigated counterparts.

While surveys exist for QEM from [[BUL21]](#BUL21) and [[END20]](#END20) our aim is to produce a more concise introduction to the topic. We look into and benchmark different QEM methods, including zero noise extrapolation (ZNE), probabilistic error cancellation (PEC), and Clifford data regression (CDR). These methods are used to extract useful computational output by combining measurement data from multiple samplings of the available imperfect quantum device. We have chosen in this report to utilize the popular Python toolkit known as [Mitiq](https://github.com/unitaryfund/mitiq) where ZNE, PEC, and CDR have already been implemented [[LAR20]](#LAR20). We also investigate other QEM techniques not included in Mitiq like quasi-probability decomposition, virtual distillation, and randomized compiling. 


## Zero Noise Extrapolation (ZNE)

During this noisy intermediate-scale quantum (NISQ) era that we are currently in there is no way around noisy results, hence the reason for the moniker. So, why not embrace the noise and use it to our benefit? Enter Zero Noise Extrapolation (ZNE), which came about in 2017 in pulse-form from [[TEM17]](#TEM17) / [[LI17]](#LI17) and appeared in "digital" gate-form from [[GIU21]](#GIU21) in 2021.

The premise is that a quantum device operates naturally at a noise level of 1 and we can increase that noise level by duplicating the circuit using various "folding" rules. By evaluating the noise-amplified circuit at different, increased levels we can then extrapolate back to the zero noise level using a classical inference technique.

When benchmarking expectation values on randomized benchmarking circuits of increasing size it is apparent that using ZNE over unmitigated values produces better results:

![](images/zne.png)

One drawback of ZNE, which should be mentioned up front, is that generally it involves using a simplistic noise model of only depolarizing noise. A more realistic noise model usually includes amplitude damping and phase damping. Another immediately apparent issue is the scalability of this technique due to the noise scaling procedure. NISQ hardware is already limited by short T1 duration (i.e. relaxation time of a qubit's information to an unusable level), so in order to perform noise scaling at higher levels the original circuit must be a relatively shallow depth.


## Probabilistic Error Cancellation (PEC)

Probabilistic error cancellation (PEC) includes using a quasi-proability system to represent an ideal circuit. Each ideal operator is decomposed into a linear combination of noisy operators that can produce the ideal result. These linear combination of noisy operators are then replaced in the original circuit. Next, the circuit is sampled in order to find the proper distribution of these operations to get output with reduced errors. The aim here is to exploit a large number of gates and utilise their statistically unbiased values around the mean to get the noise mitigated result. As expected, the accuracy of the results varies with the number of shots.

PEC has certain limitations as well. As we increase the number of gates, the sampling overhead also increases exponentially. Since it is hard to have precise noise information of the gates, usually requiring gate tomography, the implementation may turn out to be costly. Also, we need to use a large number of shots to generate statistically relevant data.


## Clifford Data Regression (CDR)

One way to inform the experimenter about noise in a quantum device is by classically simulating the circuits. The result of the Gottesman-Knill theorem [[G98]](#G98) guarantees that quantum circuits containing only Clifford gates can be simulated classically. The Clifford data regression (CDR)
method [[CACC21]](#CACC21) makes use of this result. CDR uses classically simulable training circuits consisting mainly of Clifford gates to learn effects of the noise on an observable of interest. For the training circuits the expectation value is evaluated classically and on noisy quantum hardware. This set of data is then used to perform a regressive fit on an ansatz mapping noisy to exact expectation values. The fitted ansatz is used to estimate the value of the noiseless result.
 
Recently, in [[LGCACC21]](#LGCACC21) the authors considerered the combination of CDR and ZNE which is called variable-noise CDR (vnCDR). This method can outperform either of the methods  individually. vnCDR uses training data involving near-Clifford training circuits evaluated at multiple noise levels and can be thought of as using Clifford data to inform the extrapolation to the zero noise limit.

A crucial requirement of any error mitigation method is scalability. While it is relatively easy to develop error mitigation methods for small qubit systems, error mitigation methods that work effectively at the quantum supremacy scale (> 50 qubits) are much more challenging to construct. Clifford data regression is one such method. It has the promising advantage of being a self-tuning technique since it learns about the noise during the training phase. However, constructing near-Clifford training circuits can sometimes be difficult and the evaluation of such may require a potentially large number of shots.

## Other techniques

As we mentioned in the introduction, due to the disadvantages of QEC on noise mitigation, i.e. increased qubit overhead, QEM has started to become more widespread. The popular approaches like ZNE depends on collecting data for different noise levels by amplifying the noise and then fitting an extrapolation curve for the noiseless case. However these techniques face problems especially for large problem sizes involving many noisy gates. Just as there is a trend from QEC to QEM, there is also a trend towards QEM methods that can better meet the specific needs under specific conditions. In this section we will introduce several other QEM methods discussed in the literature.

One of these techniques is quasi-probability decomposition [[CAI21]](#CAI21). In QEM techniques based on noise boosting such as ZNE, extracted noise points may not contain enough information for good extrapolation. To overcome this challange, quasi-probability decomposition technique can be used to obtain noise points, with reduced strength, to be used for noise extrapolation. 

Virtual distillation (VD) [[BUL21]](#BUL21) is another QEM technique aiming to obtain expectations of observables of a purer state by using several copies of the noisy state. Increasing the number of copies exponentially decreases the effect of contributions to the evaluated expectation coming from the eigenvectors of the noisy state other than the dominant state. The success of the VD technique depends on the noise floor and this level should be sufficiently low so that the dominant eigenvalue is close to the noiseless state. 

Using what is known as randomized compiling (RC) can convert coherent errors to incoherent errors by introducing a set of randomizing single qubit gates as introduced in [[URB21]](#URB21). RC can be used in conjuction with readout error correction and ZNE to achieve better results in terms of closeness to the exact results. In randomized compiling, an estimation circuit with a similar structure to the target circuit is executed to measure the depolarizing noise rate. Then this measured rate is used to correct the output of the target circuit. We obtained the very first results with RC using the circuit given in [[URB21]](#URB21).

![](images/zne_rc.png)

<!--
## Comparison

Currently, we are expecting to use either a QPE or MAXCUT / QAOA circuit to benchmark across the different techniques with an added constraint of a fixed shot count. The thinking behind this is that different techniques use shot resources in different ways, so it might provide an different angle by which techniques can be evaluated. Additionally, this may be a good place to introduce results from the Fundamental Limits of Quantum Error Mitigation paper.

In the meantime we are also investigating several noise models (e.g depolarizing noise), extrapolation techniques, observables and benchmarking approaches to better and fairly evaluate and compare different QEM methods.
-->

## Summary

Quantum error mitigation is a useful tool to have during this NISQ era we are in until we reach sizable quantum computing devices that can implement quantum error correction. In researching the various techniques mentioned above we have seen creative uses of the noisy hardware we currently have to improve the fidelity of expectation values of observables. The choice of technique seems to have the need to be custom tailored to the specific problem as no one technique seems to be best in all cases. Overall, this is a fertile area for future research and will be so for the years to come.

## References
<a id="END20">[END20]</a>
Suguru Endo, Zhenyu Cai, Simon C. Benjamin, and Xiao Yuan (2020). 
Hybrid quantum-classical algorithms and quantum error mitigation. 
[arXiv:2011.01382](https://arxiv.org/abs/2011.01382)

<a id="LAR20">[LAR20]</a>
Ryan LaRose, Andrea Mari, Sarah Kaiser, Peter J. Karalekas, Andre A. Alves, Piotr Czarnik, Mohamed El Mandouh, Max H. Gordon, Yousef Hindy, Aaron Robertson, Purva Thakre, Nathan Shammah, William J. Zeng (2020).
Mitiq: A software package for error mitigation on noisy quantum computers.
[arXiv:2009.04417](https://arxiv.org/abs/2009.04417)

<a id="TEM17">[TEM17]</a> 
K. Temme, S. Bravyi, and J. M. Gambetta (2017). 
Error Mitigation for ShortDepth Quantum Circuits. 
Physical Review Letters, vol. 119, p. 180509, 11 2017. 

<a id="LI17">[LI17]</a> 
Y. Li and S. C. Benjamin (2017). 
Efficient Variational Quantum Simulator Incorporating Active Error Minimization. 
Physical Review X, vol. 7, 6 2017. 

<a id="GIU21">[GIU21]</a> 
T. Giurgica-Tiron, Y. Hindy, R. LaRose, A. Mari, and W. J. Zeng (2021). 
Digital zero noise extrapolation for quantum error mitigation. 
[2020 IEEE Int. Conf. Quantum Comp. Eng. (QCE) (2020)](http://dx.doi.org/10.1109/QCE49297.2020.00045). 

<a id="CAI21">[CAI21]</a> 
Cai, Z (2021).
Multi-exponential error extrapolation and combining error mitigation techniques for NISQ applications.
Quantum Inf. vol. 80

<a id="BUL21">[BUL21]</a>
Bultrini, D., Gordon, M.H., Czarnik, P., Arrasmith, A., Coles, P.J. and Cincio, L. (2021).
Unifying and benchmarking state-of-the-art quantum error mitigation techniques.
[arXiv:2107.13470](https://arxiv.org/abs/2107.13470)

<a id="URB21">[URB21]</a>
Urbanek M, Nachman B, Pascuzzi VR, He A, Bauer CW, de Jong WA (2021).
Mitigating depolarizing noise on quantum computers with noise-estimation circuits.
[arXiv:2103.08591](https://arxiv.org/abs/2103.08591)

<a id="G98">[G98]</a>
Gottesman, D. (1998). 
The Heisenberg representation of quantum computers. arXiv preprint quant-ph/9807006.

<a id="CACC21">[CACC21]</a>
Czarnik, P., Arrasmith, A., Coles, P. J., & Cincio, L. (2021). 
Error mitigation with Clifford quantum-circuit data. Quantum, 5, 592.

<a id="LGCACC21">[LGCACC21]</a>
Lowe, A., Gordon, M. H., Czarnik, P., Arrasmith, A., Coles, P. J., & Cincio, L. (2021).
Unified approach to data-driven quantum error mitigation. 
Physical Review Research, 3(3), 033098.
<!-- #endregion -->

```python

```
