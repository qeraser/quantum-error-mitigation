 %Overleaf format from https://www.overleaf.com/project/61d31eb934374c2ff2fcedf1

%\documentclass{article}
%\documentclass[a4paper,11pt]{IEEEtran}
\documentclass[aps,twocolumn,preprintnumbers]{revtex4}
%\pdfoutput=1

% Language setting
% Replace `english' with e.g. `spanish' to change the document language
\usepackage[english]{babel}

\usepackage{graphicx}  % Include figure files
\usepackage{subfigure}
\usepackage{multirow}

% Set page size and margins
% Replace `letterpaper' with`a4paper' for UK/EU standard size
\usepackage[letterpaper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}

\begin{document}

\title{ \textbf{Benchmarking Error Mitigation Techniques\\
QCourse511 Project \#21 | Quantum Eraser}}
\author{Amir Ebrahimi, Manvi Gusain, Ranendu Adhikary, Saumya Prakash Sharma, Tugba Özbilgin}




\begin{abstract}
During the talks held in November 2021 for QJam \cite{QJ21} the basics of near-term quantum error mitigation (QEM) were presented. QEM is an active area of research with a fairly recent review that includes extensive references \cite{END21}. A relevant question to ask here is how good are these QEM techniques and how well do they perform with different noise models? See \cite{TEMG19} for a recent result on performance bounds for such techniques. We aim to benchmark a few of the popular error mitigation techniques and gain some insight. We do this by defining and justifying the metrics we use and attempt to extrapolate the scalability of each technique based on the results.

\end{abstract}

\maketitle


\section{Introduction}

Quantum computing is the encoding of information onto quantum bits (qubits) and processing that information using quantum algorithms. Quantum computers can exploit a Hilbert space whose dimension increases exponentially with the number of qubits. Mostly, quantum algorithms are developed with the assumption that qubits are perfect, often referred to as logical qubits, in that they obey quantum mechanical laws with perfect precision. Physical qubits, on the other hand, refer to the actual implementation of qubits in quantum computing devices. Unfortunately, these physical qubits suffer from imperfections like quantum noise and decoherence. Mitigation of errors is crucial to obtain reliable results. The inevitable accumulation of errors in near-term quantum devices represents a key obstacle in delivering practical quantum advantage.

Quantum error correction (QEC) was offered as a solution to mitigate the effects of quantum noise and decoherence by spreading the information of one qubit onto an entangled state over several qubits. However, QEC techniques have many drawbacks as they generally require a large overhead in terms of additional qubits on top of those required for the quantum computation. Current quantum computing devices have been able to demonstrate QEC only with a very small number of qubits \cite{AND20}. Therefore, another mitigation technique called quantum error mitigation (QEM) has arisen. QEM is generally considered as a collection of techniques that are more practical than QEC solutions as they offer low-overhead methods to more accurately and reliably estimate observable values than unmitigated counterparts.

While surveys exist for QEM from \cite{BUL21} and \cite{END21} our aim is to produce a more concise introduction to the topic. We look into and benchmark different QEM methods, including zero-noise extrapolation (ZNE), probabilistic error cancellation (PEC), and Clifford data regression (CDR). These methods are used to extract useful computational output by combining measurement data from multiple samplings of the available imperfect quantum device. We have chosen in this report to utilize the popular Python toolkit known as Mitiq where ZNE, PEC, and CDR have already been implemented \cite{LAR20}. We also investigate another QEM technique called randomized compiling (RC), which is not included in Mitiq.

\section{Zero Noise Extrapolation (ZNE)}
During this noisy, intermediate-scale quantum (NISQ) era that we are currently in there is no way around noisy results, hence the reason for the moniker. So, why not embrace the noise and use it to our benefit? Enter zero-noise extrapolation (ZNE), which came about in 2017 in pulse-form from \cite{TEM17, LI17} and appeared in ``digital" gate-form from \cite{GIU21} in 2021.

The premise is that a quantum device operates naturally at a noise level of 1 and we can increase that noise level by duplicating the circuit using various ``folding" rules. By evaluating the noise-amplified circuit at different, increased levels we can then extrapolate back to the zero noise level using a classical inference technique.

When plotting expectation values on randomized benchmarking circuits of increasing size it is apparent that using ZNE over unmitigated values produces better results as seen in Figure \ref{fig:zne}.

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{images/zne_rb.png}
\caption{\label{fig:zne}ZNE.}
\end{figure}

ZNE benchmarks usually show performance using a simplistic noise model of only depolarizing noise. A more realistic noise model usually includes amplitude damping and phase damping. Another immediately apparent issue is the scalability of this technique due to the noise scaling procedure. NISQ hardware is already limited by short T1 duration (i.e. relaxation time of a qubit's information to an unusable level), so in order to perform noise scaling at higher levels the original circuit must be of a relatively shallow depth.


\section{Probabilistic Error Cancellation (PEC)}

Probabilistic error cancellation (PEC) is an approach that uses a quasi-probability system to represent an ideal circuit. Each ideal operator is decomposed into a linear combination of noisy operators that can produce the ideal result. These linear combinations of noisy operators are then replaced in the original circuit. Next, the circuit is sampled in order to find the proper distribution of these operations to get output with reduced errors. The aim here is to exploit a large number of gates and utilise their statistically unbiased values around the mean to get the noise mitigated result. As expected, the accuracy of the results varies with the number of shots.

PEC has certain limitations as well. As we increase the number of gates, the sampling overhead also increases exponentially to maintain precision. Since it is hard to have precise noise information of the gates, usually gate tomography is required, which is costly to collect. Furthermore, we need to use a large number of shots to generate statistically relevant data. Figure \ref{fig:pec} shows the costly scaling in number of samples as the circuit depth increases.

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{images/pec_rb.png}
\caption{\label{fig:pec}PEC w/ precision values [0.2, 0.4, and 0.6] corresponding to the number of clifford gates; resulting sample counts are [62, 406, 16380], which exhibits the exponential scaling.}
\end{figure}

%Use the table and tabular environments for basic tables --- see Table~\ref{tab:widgets}, for example. For more information, please see this help article on \href{https://www.overleaf.com/learn/latex/tables}{tables}. 

%\begin{table}
%\centering
%\begin{tabular}{l|r}
%Item & Quantity \\\hline
%Widgets & 42 \\
%Gadgets & 13
%\end{tabular}
%\caption{\label{tab:widgets}An example table.}
%\end{table}

\section{Clifford Data Regression (CDR)}

One way to inform the experimenter about noise in a quantum device is by classically simulating the circuit and comparing results. The result of the Gottesman-Knill theorem \cite{G98} guarantees that quantum circuits containing only Clifford gates can be simulated classically in an efficient manner. The Clifford data regression (CDR) method \cite{CACC21} makes use of this result. CDR generates near-Clifford training circuits from an initial circuit in order to classically simulate and learn the effects of the noise on an observable of interest. For the training circuits the expectation value is evaluated classically and on noisy quantum hardware. This set of data is then used to perform a regressive fit on an ansatz mapping noisy to exact expectation values. The fitted ansatz is used to estimate the value of the noiseless result.

Although randomized benchmarking circuits utilize Clifford gates and can be simulated classically it is still interesting to verify that CDR produces the exact results even with noisy execution as seen in Figure \ref{fig:cdr}.

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{images/cdr_rb.png}
\caption{\label{fig:cdr}CDR.}
\end{figure}

Recently, in \cite{LGCACC21} the authors considered the combination of CDR and ZNE which is called variable-noise CDR (vnCDR). This method can outperform either of the methods individually. vnCDR uses training data involving near-Clifford training circuits evaluated at multiple noise levels and can be thought of as using Clifford data to inform the extrapolation to the zero noise limit.

A crucial requirement of any error mitigation method is scalability. While it is relatively easy to develop error mitigation methods for small qubit systems, error mitigation methods that work effectively at the quantum supremacy scale ($> 50$ qubits) are much more challenging to construct. Clifford data regression is one such method. It has the promising advantage of being a self-tuning technique since it learns about the noise during the training phase. However, constructing near-Clifford training circuits can sometimes be difficult and the evaluation of such may require a potentially large number of shots.


\section{Randomized Compiling (RC)}

Gate errors are one of the major sources of noise in quantum circuits and can be classified in two groups: Coherent and incoherent errors. Coherent errors occur due to some miscalibration of circuit parameters and do not destroy the state purity. This can be a serious problem since similar errors occur in consecutive executions leading to a bias. It is easier to handle incoherent errors using what is known as randomized compilign (RC). We can convert coherent errors to incoherent errors by introducing a set of randomizing single qubit gates as introduced in \cite{URB21}. RC can be used in conjunction with readout error correction and other QEM techniques like ZNE to achieve better results in terms of closeness to the exact results. In RC, an estimation circuit with a similar structure to the original circuit that has a known output is executed to measure the depolarizing noise rate. Then, this measured rate is used to correct the output of the target circuit. 

In RC, the expectation value of the observable is corrected by scaling it with $1-p$ as given in (\ref{rc_obs}), where $p$ is the noise rate evaluated using the estimation circuit.
Using what is known as randomized compiling (RC) we can convert coherent errors to incoherent errors by introducing a set of randomized single qubit gates.

\begin{equation} \label{rc_obs}
\langle  O \rangle  = \frac{\bar{\langle  O \rangle }}{1-p} 
\end{equation}

The estimation circuit is constructed by removing all single-qubit gates from the original circuit and just preserving the CNOT gates. The idea lies behind the fact that only CNOT gates are the principal source of gate errors. By removing the gates one can be sure of the output of the circuit and thereby exactly calculate the probability of error. For each CNOT gate two randomizing gates (identity or Pauli gates, i.e. I, X, Y or Z gates) are added in front of the gate and another two after the gate. These four gates are chosen such that the overall impact of the gates are exactly that of a CNOT gate. This randomized compiling procedure is applied to both the original and the estimation circuit. Additionally to make the system more robust, layers of random rotations are added before and the inverse of these rotations are added after the original circuit. A sample of original and estimation circuits can be seen in Fig. \ref{fig:org_est_circuits}.


\begin{figure}[!htbp]
\centering
\includegraphics[width=0.25\textwidth]{images/org_circuit.png}
\includegraphics[width=0.5\textwidth]{images/tar_circuit.png}
\includegraphics[width=0.5\textwidth]{images/est_circuit.png}
\caption{\label{fig:org_est_circuits} Original circuit, rotation layers added to the target circuit, and rotation layers added, single-qubit gates removed and randomized compiling applied to the estimation circuit.}
\end{figure} 

The RC technique can be used in together with the other QEM techniques to improve their performances. In\cite{URB21} and \cite{ROS21}, RC is used with ZNE. As stated in \cite{ROS21} the technique known as ZNE-first is applied as follows: ZNE is applied both to the original and estimation circuits and the noise rate is predicted by dividing the former value to the latter one. 

\section{Other techniques}

As we mentioned in the introduction, due to the disadvantages of QEC, i.e. increased qubit overhead, QEM has started to become more widespread. Popular approaches like ZNE depend on collecting data for different noise levels by amplifying the noise and then fitting an extrapolation curve for the noiseless case. However, these techniques face problems especially for large problem sizes involving many noisy gates. Just as there is a trend from QEC to QEM, there is also a trend towards QEM methods that can better meet the specific needs under specific conditions. In this section we will introduce several other QEM methods discussed in the literature.

One of these techniques is quasi-probability decomposition \cite{CAI21}. In QEM techniques based on noise scaling, such as ZNE, extracted noise points may not contain enough information for good extrapolation. To overcome this challenge, the quasi-probability decomposition technique can be used to obtain noise points with reduced strength to be used for noise extrapolation.

There are also novel QEM techniques proposed, such as virtual distillation (VD) \cite{BUL21} and quantum subspace expansion (QSE) \cite{YOS21}, that do not require any prior knowledge about error models. VD is a QEM technique aiming to obtain expectations of observables of a purer state by using several copies of the noisy state. Increasing the number of copies exponentially decreases the effect of contributions to the evaluated expectation coming from the eigenvectors of the noisy state other than the dominant state. The success of the VD technique depends on the noise floor and this level should be sufficiently low so that the dominant eigenvalue is close to the noiseless state.

The other error-agnostic technique, QSE, on the other hand, uses an expanded subspace around an approximated ground state. Post-processing of the classical data using at worst additional measurements with simple gates is done. The result is then inherently error mitigated because the states are approximated as a linear combination of these states in the expanded subspace.



%You can make lists with automatic numbering \dots

%\begin{enumerate}
%\item Like this,
%\item and like this.
%\end{enumerate}
%\dots or bullet points \dots
%\begin{itemize}
%\item Like this,
%\item and like this.
%\end{itemize}

\section{Comparison} \label{sec:comparison}

Most literature on QEM makes use of a simple noise model for evaluating the techniques covered -- depolarizing noise channels. However, most quantum devices don't exhibit this simple type of noise and have additional noise influences (e.g. cross-talk among qubits). For reference, a depolarizing noise channel is:

\begin{equation} \label{depolarizing_noise}
\Delta_\lambda(\rho) = (1 - \lambda)\rho + \frac{\lambda}{d}I
\end{equation}

where depending on the noise level $\lambda$ either the state $\rho$ or a regression towards the maximally mixed state away from $\rho$ is returned.

We find it interesting to explore other noise models individually to see how each technique performs. Specifically, in addition to depolarizing noise we look at phase flips, bit flips, phase damping, and amplitude damping in isolation. As mentioned in the previous section it is seemingly necessary to have custom-tailored QEM that accounts for specific quantum algorithms running on specific quantum hardware.

For our comparisons, we will use a Quantum Phase Estimation (QPE) circuit, since QPE is considered one of the most important subroutines in quantum computation \cite{PAR11}. QPE consists of estimating the phase, $e^{2 \pi i \theta}$, for an eigenvector, $|\psi\rangle$, of a unitary operator $U$ (i.e. $U|\psi\rangle = e^{2 \pi i \theta}|\psi\rangle$). For our QPE circuit we use 5 qubits -- 1 qubit for $|\psi\rangle$ and 4 'counting' qubits; resulting in a circuit depth of 17. For the phase we use $\frac{5}{16}$ ($0.3125$), which an exact simulation of the circuit will return the value $5$ with probability 1. Mitiq allows creating observables with a linear combination of Pauli strings. To have our observable match the results from measuring the 'counting' qubits we utilize the following:

\begin{equation} \label{qpe_obs}
7.5I + -4Z_0 + -2Z_1 + -Z_2 + -0.5Z_3
\end{equation}

\subsection{ZNE}

Starting with ZNE, let's compare the noise models using noise strengths $[0.02, 0.04, 0.06, 0.08, 0.1]$ for each model and Richardson extrapolation with noise scaling levels at $[1.0, 3.0, 5.0, 7.0]$. Expectation values for each noise model are averaged over $25$ runs with $1024$ shots on each run. 

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{images/zne_noise_models.png}
\caption{\label{fig:zne_nm}ZNE across different noise models.}
\end{figure}

As seen in Figure \ref{fig:zne_nm} there are two distinct groups -- phase / amplitude damping and depolarizing noise, bit / phase flips. Phase and amplitude damping seem to be supported best by ZNE for QPE with around $0.5$ in absolute error for the expectation value even as noise strength approaches $10\%$. The other noise models seem to scale worse, albeit linearly, as the noise strength increases.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{images/zne_noise_scaling.png}
\caption{\label{fig:zne_ns}ZNE noise scaling across different noise models.}
\end{figure}

Specifically for ZNE it can be interesting to look at the noise scaling as the circuit is folded many times to create additional data points. As seen in Figure \ref{fig:zne_ns} the error in expectation value seems to plateau for depolarizing, phase, and bit flips. If higher noise scale factors were used it would likely cause more error. In the case of phase and amplitude damping we can see less of a plateau, which matches with the lower error we saw in the previous figure from Richardson extrapolation.



\subsection{PEC}
Next, let's take a look at PEC. As PEC includes creating quasi-probability operators for each gate in the circuit, the total number of such representations increases, which subsequently increases the execution time. Another reason for this is the sampling overheads that, too, increase with the number of gates. That's why we have resided to the noise strengths of $[0.002, 0.004, 0.006, 0.008, 0.01] $ for each of the noise models.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{images/pec.jpg}
\caption{\label{fig:pec}PEC noise across different noise models. \textit{Note: there is high variation among reproductions of this graph, which requires further investigation.}}
\end{figure}


\subsection{CDR}

Now, let's take a look at CDR. We have studied mainly five noise models -- amplitude damping, phase flip, phase damping, bit flip and depolarizing noise. We have compared these noise models over their noise strengths $[0.02, 0.04, 0.06, 0.08, 0.1]$ using $30$ training Clifford circuits. The absolute errors are averaged over $50$ runs with $5000$ shots on each run (Figure \ref{fig:cdr305000}).


\begin{figure}[!htbp]
\centering
\includegraphics[width=0.5\textwidth]{images/CDR305000.jpg}
\caption{\label{fig:cdr305000}CDR across different noise models. \textit{Note: there is high variation among reproductions of this graph, which requires further investigation.}}
\end{figure}

%\begin{figure}[!htbp]
%\centering
%\includegraphics[width=0.45\textwidth]{images/cdr_ad_5000.jpg}
%\includegraphics[width=0.45\textwidth]{images/cdr_bf_5000.jpg}
%\includegraphics[width=0.45\textwidth]{images/cdr_dp_5000.jpg}
%\includegraphics[width=0.45\textwidth]{images/cdr_pf_5000.jpg}
%\includegraphics[width=0.45\textwidth]{images/cdr_pd_5000.jpg}
%\caption{\label{fig:cdr_tr}CDR across different noise models with different training circuits.}
%\end{figure} 




As seen in Figure \ref{fig:cdr305000}, the absolute errors become approximately the same for a noise level of $0.1$ for all the noise models except phase damping noise. The absolute error grows approximately constant for phase damping noise with an increasing amount of noise.


%In Figure \ref{fig:cdr_tr} we have plotted each noise model with noise strengths $[0.02, 0.04, 0.06, 0.08, 0.1]$ with respect to different number of training Clifford circuits. Expectation values for each noise model are averaged over $50$ runs with $5000$ shots on each run.



%As seen in Figure \ref{fig:cdr_tr}, for amplitude damping, phase damping and phase flip noise models CDR will give approximately the same expectation values for $10$ and $50$ training circuits.



\subsection{vnCDR}

Now, let's take a look at vnCDR. Like CDR we make the same types of comparisons in vnCDR. We compare these noise models over the noise levels $[0.02, 0.04, 0.06, 0.08, 0.1]$ using $30$ training Clifford circuits scaling taking the noise scaling to be $5$. The absolute errors are averaged over $50$ runs with $5000$ shots on each run (Figure \ref{fig:vncdr305000}).

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.5\textwidth]{images/vnCDR across_diff_n_w_30tc and 5000 shots.jpg}
\caption{\label{fig:vncdr305000}vnCDR across different noise models. \textit{Note: there is high variation among reproductions of this graph, which requires further investigation.}}
\end{figure}

As seen in Figure \ref{fig:vncdr305000}, the gap between absolute errors for each noise level decreases with increasing noise strengths.


\subsection{ZNE, PEC and CDR with RC}
We first obtain results with RC using the circuit given in the Mitiq tutorial for the CDR technique. To make the circuit longer we repeat the given circuit several times. For the quality of demonstration we use 3 repetitions for ZNE-RC and PEC-RC and 7 repetitions for CDR-RC. For this reason we show results for the former on the same figure as given in Fig. \ref{fig:zne_pec_rc_dep} and the latter on Fig. \ref{fig:cdr_rc}. Here we use the normalized error as the performance indicator. As can be deduced from both figures, RC enhances the performance of ordinary QEM techniques.


% \begin{figure}
% \centering
% \includegraphics[width=0.5\textwidth]{images/zne_rc.png}
% \caption{\label{fig:zne_rc}ZNE RC.}
% \end{figure}


\begin{figure}[!h]
\centering
\includegraphics[width=0.5\textwidth]{images/zne_pec_rc_dep.png}
\caption{\label{fig:zne_pec_rc_dep} ZNE and PEC error results with RC under depolarize noise model.}
\end{figure}


\begin{figure}[!h]
\centering
\includegraphics[width=0.5\textwidth]{images/cdr_rc.png}
\caption{\label{fig:cdr_rc} CDR error results with RC under depolarize noise model.}
\end{figure}

We also consider the phase flip noise model to test whether the system still works as desired. As shown in Fig. \ref{fig:zne_rc_phaseflip}, the RC technique brings error reduction in the phase flip noise model, too.

\begin{figure}[!h]
\centering
\includegraphics[width=0.5\textwidth]{images/zne_rc_phaseflip.png}
\caption{\label{fig:zne_rc_phaseflip} ZNE error results with RC under phase flip noise model.}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=0.5\textwidth]{images/zne_rc_qpe_dep.png}
\caption{\label{fig:zne_rc_qpe_dep} ZNE error results with RC under depolarize noise model using the QPE circuit.}
\end{figure}

Finally, let's take a look at a the performance of RC using our QPE circuit. We used the QPE circuit given at the top of this Section \ref{sec:comparison}. The depths of the original, target and estimation circuits are 58, 51 and 62, respectively and so we can say that the complexity of the circuits are similar. We use 1024 shots and obtain the results by taking the average of 15 runs. The absolute error results (i.e. out of 5, without normalization) under depolarizing noise is given in Fig. \ref{fig:zne_rc_qpe_dep}. We observe that RC successfully increases the performance of the ZNE technique at relatively high noise levels at the cost of calculation of two ZNE expectations instead of one. 




% Currently, we are expecting to use either a QPE or MAXCUT / QAOA circuit to benchmark across the different techniques with an added constraint of a fixed shot count. The thinking behind this is that different techniques use shot resources in different ways, so it might provide an different angle by which techniques can be evaluated. Additionally, this may be a good place to introduce results from \cite{TEMG19}.


\section{Summary}

Quantum error mitigation is a useful tool to have during this NISQ era that we are in until we reach sizable quantum computing devices that can implement quantum error correction. In researching the various techniques mentioned above, we have seen creative uses of the noisy hardware we currently have to improve the fidelity of expectation values of observables. The choice of technique seems to have the need to be custom tailored to the specific problem as no one technique seems to be best in all cases. As we keep evaluating the performances by implementing different problems, newer inferences are expected to be observed. Not much is known about combining near-term error mitigation with textbook error correction techniques, although \cite{PSB21} has recently explored this. Overall, this is a fertile area for future research and will be so for the years to come.


%\bibliographystyle{alpha}
\begin{thebibliography}{9}

\bibitem{QJ21}
Yakaryilmaz, Abuzer. QWorld QJam2021. https://qworld.net/qjam2021/.

\bibitem{END21}
Endo, S., Cai, Z., Benjamin, S.C. and Yuan, X., 2021. Hybrid quantum-classical algorithms and quantum error mitigation. Journal of the Physical Society of Japan, 90(3), p.032001.

\bibitem{TEMG19}
Takagi, R., Endo, S., Minagawa, S., and Gu, M. (2021). Fundamental limits of quantum error mitigation. arXiv:2109.04457.

\bibitem{PSB21}
Piveteau, C., Sutter, D., Bravyi, S., Gambetta, J.M. and Temme, K., 2021. Error mitigation for universal gates on encoded qubits. arXiv:2103.04915.

\bibitem{AND20}
Andersen, C.K., Remm, A., Lazar, S. et al. Repeated quantum error detection in a surface code. Nat. Phys. 16, 875–880 (2020). https://doi.org/10.1038/s41567-020-0920-y

\bibitem{LAR20}
Ryan LaRose, Andrea Mari, Sarah Kaiser, Peter J. Karalekas, Andre A. Alves, Piotr Czarnik, Mohamed El Mandouh, Max H. Gordon, Yousef Hindy, Aaron Robertson, Purva Thakre, Nathan Shammah, William J. Zeng (2020).
Mitiq: A software package for error mitigation on noisy quantum computers.
arXiv:2009.04417


\bibitem{TEM17}K. Temme, S. Bravyi, and J. M. Gambetta (2017). 
Error Mitigation for ShortDepth Quantum Circuits. Physical Review Letters, vol. 119, p. 180509, 11 2017.

\bibitem{LI17}
Y. Li and S. C. Benjamin (2017). 
Efficient Variational Quantum Simulator Incorporating Active Error Minimization. 
Physical Review X, vol. 7, 6 2017. 

\bibitem{GIU21}
T. Giurgica-Tiron, Y. Hindy, R. LaRose, A. Mari, and W. J. Zeng (2021). 
Digital zero noise extrapolation for quantum error mitigation. 
2020 IEEE Int. Conf. Quantum Comp. Eng. (QCE) (2020) http://dx.doi.org/10.1109/ QCE49297.2020.00045. 

\bibitem{CAI21}
Cai, Z (2021).
Multi-exponential error extrapolation and combining error mitigation techniques for NISQ applications.
Quantum Inf. vol. 80

\bibitem{BUL21}
Bultrini, D., Gordon, M.H., Czarnik, P., Arrasmith, A., Coles, P.J. and Cincio, L. (2021).
Unifying and benchmarking state-of-the-art quantum error mitigation techniques.
arXiv:2107.13470

\bibitem{YOS21}
Yoshioka, N., Hakoshima, H., Matsuzaki, Y., Tokunaga, Y., Suzuki, Y. and Endo, S., 2021. Generalized quantum subspace expansion. arXiv preprint arXiv:2107.02611.

\bibitem{URB21}
Urbanek M, Nachman B, Pascuzzi VR, He A, Bauer CW, de Jong WA (2021).
Mitigating depolarizing noise on quantum computers with noise-estimation circuits.
arXiv:2103.08591

\bibitem{ROS21}
Rosenberg, E., Ginsparg, P. and McMahon, P.L., 2021. Experimental error mitigation using linear rescaling for variational quantum eigensolving with up to 20 qubits. arXiv preprint arXiv:2106.01264.

\bibitem{G98}
Gottesman, D. (1998). 
The Heisenberg representation of quantum computers. arXiv preprint quant-ph/9807006.

\bibitem{CACC21}
Czarnik, P., Arrasmith, A., Coles, P. J., and Cincio, L. (2021). 
Error mitigation with Clifford quantum-circuit data. Quantum, 5, 592.

\bibitem{LGCACC21}
Lowe, A., Gordon, M. H., Czarnik, P., Arrasmith, A., Coles, P. J., and Cincio, L. (2021).
Unified approach to data-driven quantum error mitigation. 
Physical Review Research, 3(3), 033098.

\bibitem{PAR11}
Parasa, Vamsi and Marek A. Perkowski. “Quantum Phase Estimation Using Multivalued Logic.” 2011 41st IEEE International Symposium on Multiple-Valued Logic (2011): 224-229.

\end{thebibliography}

\end{document}