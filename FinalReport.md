# Final Project Report

## Introduction

<!-- Introduce your project: introduction and motivation, team member(s), available work/literature and your proposal, the list of deliveriables, etc. -->

Our project was inspired by [this QJam 2021 project suggestion](https://gitlab.com/qworld/qjam/2021/-/blob/main/project_ideas/Benchmarking_Error_Mitigation.md), which was not attempted during QJam 2021. Near-term quantum error mitigation (QEM) is quite an active area of research and as a group we wished to explore how good these error mitigation techniques perform. We numerically benchmarked these techniques against different noise models and in the process learned about what metrics were important to track. We made use of [Mitiq](https://github.com/unitaryfund/mitiq), an open-source toolkit, to perform these benchmarks. The QEM techniques we used were zero-noise extrapolation, Clifford-data regression (CDR), probabilistic error cancellation (PEC) and randomized compiling (RC).  

### Members
Amir Ebrahimi, @amirebrahimi  
Manvi Gusain, @Manvi333  
Ranendu Adhikary, @ronjumath  
Saumya Prakash Sharma, @saumya.sharma1  
Tugba Özbilgin, @tozbilgin  

### Resources
We primarily used the following literature in support of our project:
- [Hybrid quantum-classical algorithms and quantum error mitigation](https://arxiv.org/abs/2011.01382)
- [Mitiq: A software package for error mitigation on noisy quantum computers](https://arxiv.org/abs/2009.04417)
- [Unifying and benchmarking state-of-the-art quantum error mitigation techniques](https://arxiv.org/abs/2107.13470)
- [Fundamental limits of quantum error mitigation](https://arxiv.org/abs/2109.04457)
- [Mitigating depolarizing noise on quantum computers with noise-estimation circuits](https://arxiv.org/abs/2103.08591)

### Deliverables
Our final deliverable was a technical report formatted in LaTeX with Overleaf.

As a stretch goal, an [initial attempt](https://github.com/amirebrahimi/mitiq/tree/rci) at [readout confusion inversion](https://github.com/unitaryfund/mitiq/issues/815) was started in order to contribute back to Mitiq.



## Outcomes

<!--Outcomes of the project and their explanations.-->

For evaluation of our project we present the following:

[Technical report (PDF)](BenchmarkingQEM.pdf)  
[Presentation video](https://youtu.be/UasJ_Mt0P_4)  
[Presentation slides](BenchmarkingQEM_Presentation.pdf)  
[Jupyter notebooks](https://gitlab.com/qeraser/quantum-error-mitigation/)



## Discussion

<!-- Discussions: Success of the implementations, the importance of the outcomes, the contibution of the project, originality of work, etc.)-->

For our comparisons, we used a Quantum Phase Estimation (QPE) circuit, since QPE is considered one of the most important subroutines in quantum computation. QPE consists of estimating the phase, <!--$e^{2 \pi i \theta}$--> e^(2πiθ), for an eigenvector, <!--$|\psi\rangle$--> ∣ψ⟩, of a unitary operator <!--$U$--> _U_ (i.e. <!--$U|\psi\rangle = e^{2 \pi i \theta}|\psi\rangle$--> U∣ψ⟩=e^(2πiθ)
 ∣ψ⟩). For our QPE circuit we used 5 qubits -- 1 qubit for <!--$|\psi\rangle$-->∣ψ⟩ and 4 'counting' qubits; resulting in a circuit depth of 17. For the phase we used <!--$\frac{5}{16}$ ($0.3125$)--> 5/16 (0.3125), which an exact simulation of the circuit will return the value <!--$5$--> 5 with probability 1. Mitiq allows creating observables with a linear combination of Pauli strings. To have our observable match the results from measuring the 'counting' qubits we utilized the following:

<!--$$
7.5I + -4Z_0 + -2Z_1 + -Z_2 + -0.5Z_3
$$-->
7.5I + -4Z(0) + -2Z(1) + -Z(2) + -0.5Z(3)

Benchmarking ZNE across different noise models proved to be straightforward. The process took ~2 hours to complete. Benchmarking the CDR and PEC techniques proved to be quite difficult and we were unable to produce consistent results between different executions. These processes took much longer (4-6 hours) and could easily run out of memory if the wrong parameters were used. In the end it was hard to draw any conclusions from the data for CDR and PEC. We simply ran out of time.

RC promises error reduction through conversion of any coherent errors to incoherent errors by introducing a set of randomizing single qubit gates. We implemented RC in conjunction with Mitiq's ZNE, CDR and PEC solutions and showed performance improvements in terms of error reduction for all three of these techniques (utilizing a depolarizing noise model only).

Benchmarking error mitigation techniques are important and it seems to be necessary to custom tailor these to the hardware and algorithm to get good results. While our work is not particularly original we have created a more concise technical report as an introduction to QEM and started an exploration into benchmarking against other noise models other than the simplistic depolarizing noise model. 



## Conclusion

<!--Concluding remarks: summary of project, challenges during the implementations and how to overcome them, whether the outcomes can be extended and possible future work, etc.)-->

The QEM techniques we considered have many parameters that affect the performance of the system. These include different noise models (i.e. depolarizing, phase flips, bit flips, phase damping, amplitude damping), scale factor for ZNE, number of samples for PEC, etc. Additionally we also had concerns about selection of the benchmarking circuit, depth of the circuit, observables, number of shots and number of runs to obtain smoother curves through averaging. Each simulation took quite a long time. Some outcomes were satisfactory while others were unsatisfactory due to inconsistency with being able to reproduce. Being that the project was only a month's time and we were all learning about the theory behind each QEM technique, we believe that our outcomes overall were satisfactory. There is still much that we could do with this project to contribute better results. Resolving the inconsistencies with PEC and CDR would be the first step. So, this project could continue for us or potentially others as a future QResearch project.

Before and during the project we contacted Sumeet Katri and M. Sohaib Alam as mentors and had calls with both in order to determine what area to focus on and to help steer the project in a good direction. We are grateful for their support.

It's worth mentioning that although none of us will receive  credit for this course through our universities, we still have worked incredibly hard ont he project simply because we deem quantum computing a fun endeavour.




## Individual Contributions

The following section has been written by each of the participants, individually, and is in the alphabetical order of first names.

### Amir Ebrahimi
[Video link](https://youtu.be/pt6r6G09cU8)
- Initial project proposal write-up
- Interfaced with Mitiq team for questions we had
- Introduction (section I)
- Zero Noise Extrapolation (ZNE) section II
- Comparison section VII introduction
- Observable for QPE (mentioned in section VII) 
- ZNE sub-section VII-A
- Final report template, introduction, QPE and comparison discussion, summary (co-authored)
- Troubleshooting PEC with randomized benchmarking and QPE circuits
- ZNE section of presentation + editing
- Started on an [implementation](https://github.com/amirebrahimi/mitiq/tree/rci) of [RCI](https://github.com/unitaryfund/mitiq/issues/815) for Mitiq

### Manvi Gusain
[Video link](https://www.youtube.com/watch?v=HsgXHkLq9WQ)
- Introduction (section I)  
- ZNE, PEC and CDR with RC (sub-section VII-E)
- Implementation of RC with CDR and PEC
- Presentation template

### Ranendu Adhikary
[Video link](https://drive.google.com/file/d/180-Am1VvCL92v0vRBey-wrR9h0NzD3DE/view?usp=sharing)
- Clifford Data Regression (CDR) (section IV)
- variable-noise Clifford Data Regression (vnCDR) (section IV)
- CDR (sub-section VII-C)
- vnCDR (sub-section VII-D)

### Saumya Prakash Sharma
[Video link](https://drive.google.com/file/d/1TGJaP4evGmyw2CNldXAYGJLRN6UK_53G/view?usp=sharing)  
- Probabilistic Error Cancellation (PEC) (section I)  
- PEC (sub-section VII-B)  
- Summary (co-authored)  
- Presentation content for slides  

### Tugba Özbilgin
[Video link](https://www.youtube.com/watch?v=A9iVMyZRMcI)  
- Literature survey for considering alternative QEM techniques not included in Mitiq
- Introduction (section I)
- Randomized Compiling (RC) (section V)
- Other Techniques (section VI)
- ZNE, PEC and CDR with RC (sub-section VII-E)
- Final paper Overleaf document template
- Randomized compiling code
- Implementation of RC with ZNE
- A unified RC code considering all ZNE, CDR and PEC
- Final report, presentation



## References

Our overall list of references from our technical report were:

1. Yakaryilmaz, Abuzer. QWorld QJam2021. https://qworld.net/qjam2021/.

1. Endo, S., Cai, Z., Benjamin, S.C. and Yuan, X., 2021. Hybrid quantum-classical algorithms and quantum error mitigation. Journal of the Physical Society of Japan, 90(3), p.032001.

1. Takagi, R., Endo, S., Minagawa, S., and Gu, M. (2021). Fundamental limits of quantum error mitigation. arXiv:2109.04457.

1. Piveteau, C., Sutter, D., Bravyi, S., Gambetta, J.M. and Temme, K., 2021. Error mitigation for universal gates on encoded qubits. arXiv:2103.04915.

1. Andersen, C.K., Remm, A., Lazar, S. et al. Repeated quantum error detection in a surface code. Nat. Phys. 16, 875–880 (2020). https://doi.org/10.1038/s41567-020-0920-y

1. Ryan LaRose, Andrea Mari, Sarah Kaiser, Peter J. Karalekas, Andre A. Alves, Piotr Czarnik, Mohamed El Mandouh, Max H. Gordon, Yousef Hindy, Aaron Robertson, Purva Thakre, Nathan Shammah, William J. Zeng (2020).
Mitiq: A software package for error mitigation on noisy quantum computers.
arXiv:2009.04417

1. K. Temme, S. Bravyi, and J. M. Gambetta (2017). 
Error Mitigation for ShortDepth Quantum Circuits. Physical Review Letters, vol. 119, p. 180509, 11 2017.

1. Y. Li and S. C. Benjamin (2017). 
Efficient Variational Quantum Simulator Incorporating Active Error Minimization. 
Physical Review X, vol. 7, 6 2017. 

1. T. Giurgica-Tiron, Y. Hindy, R. LaRose, A. Mari, and W. J. Zeng (2021). 
Digital zero noise extrapolation for quantum error mitigation. 
[2020 IEEE Int. Conf. Quantum Comp. Eng. (QCE) (2020)](http://dx.doi.org/10.1109/QCE49297.2020.00045). 

1. Cai, Z (2021).
Multi-exponential error extrapolation and combining error mitigation techniques for NISQ applications.
Quantum Inf. vol. 80

1. Bultrini, D., Gordon, M.H., Czarnik, P., Arrasmith, A., Coles, P.J. and Cincio, L. (2021).
Unifying and benchmarking state-of-the-art quantum error mitigation techniques.
[arXiv:2107.13470](https://arxiv.org/abs/2107.13470)

1. Yoshioka, N., Hakoshima, H., Matsuzaki, Y., Tokunaga, Y., Suzuki, Y. and Endo, S., 2021. Generalized quantum subspace expansion. arXiv preprint arXiv:2107.02611.

1. Urbanek M, Nachman B, Pascuzzi VR, He A, Bauer CW, de Jong WA (2021).
Mitigating depolarizing noise on quantum computers with noise-estimation circuits.
arXiv:2103.08591

1. Rosenberg, E., Ginsparg, P. and McMahon, P.L., 2021. Experimental error mitigation using linear rescaling for variational quantum eigensolving with up to 20 qubits. arXiv preprint arXiv:2106.01264.

1. Gottesman, D. (1998). 
The Heisenberg representation of quantum computers. arXiv preprint quant-ph/9807006.

1. Czarnik, P., Arrasmith, A., Coles, P. J., and Cincio, L. (2021). 
Error mitigation with Clifford quantum-circuit data. Quantum, 5, 592.

1. Lowe, A., Gordon, M. H., Czarnik, P., Arrasmith, A., Coles, P. J., and Cincio, L. (2021).
Unified approach to data-driven quantum error mitigation. 
Physical Review Research, 3(3), 033098.

1. Parasa, Vamsi and Marek A. Perkowski. “Quantum Phase Estimation Using Multivalued Logic.” 2011 41st IEEE International Symposium on Multiple-Valued Logic (2011): 224-229.