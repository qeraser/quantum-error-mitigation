import cirq
from cirq.circuits import InsertStrategy
from cirq import H, X, SWAP, CZPowGate
import math

def iqft(n,qubits,circuit):
    
    #Swap the qubits
    for i in range(n//2):
        circuit.append(SWAP(qubits[i],qubits[n-i-1]), strategy = InsertStrategy.NEW)
     
    #For each qubit
    for i in range(n-1,-1,-1):
        #Apply CR_k gates where j is the control and i is the target
        k=n-i #We start with k=n-i
        for j in range(n-1,i,-1):
            #Define and apply CR_k gate
            crk = CZPowGate(exponent = -2/2**(k))
            circuit.append(crk(qubits[j],qubits[i]),strategy = InsertStrategy.NEW)
            k=k-1 #Decrement at each step
            
        #Apply Hadamard to the qubit
        circuit.append(H(qubits[i]),strategy = InsertStrategy.NEW)

def qpe(t, control, target, circuit, CU):
    
    #Apply Hadamard to control qubits
    circuit.append(cirq.H.on_each(control))
    
    #Apply CU gates
    for i in range(t):
        #Obtain the power of CU gate 
        CUi = CU**(2**i)
        #Apply CUi gate where t-i-1 is the control
        circuit.append(CUi(control[t-i-1],*target))
        
    #Apply inverse QFT
    iqft(t,control,circuit)
    
def generate_qpe_circuit(U, n, eps=None, t=None):
    """
    Generate a QPE circuit for a unitary U that estimates 𝜙 accurate to 𝑛 bits with probability of success at least 1−𝜖 (eps).
        𝜙 here is the phase in the eigenvalue, 𝑒^{2𝜋𝑖𝜙}, for the eigenvector |𝜓⟩
        
        Args:
            U: unitary
            n: bits of precision
            eps: error probability (defaults to 1/3)
            t: "counting" qubits (defaults to 𝑡=𝑛+⌈log(2+1/(2𝜀)⌉)
            
        Returns:
            circuit (w/o measurement), t, control, target
    """
    
    CU = U.controlled()
    
    if eps is not None and t is not None:
        raise Exception("Cannot pass both eps and t, since t is determined by eps")

    if eps is None:
        eps = 1/3
        
    if t is None:
        t = n + math.ceil(math.log(2 + 1/(2*eps), 2))
    
    #Create cirucit
    circuit = cirq.Circuit()

    #Create t control qubits
    control = [cirq.LineQubit(i) for i in range(t) ]

    #Create n target qubits
    target = [cirq.LineQubit(i) for i in range(t,t+n) ]

    #Set target qubit to state |1> 
    circuit.append(X.on_each(target))

    #Apply QPE
    qpe(t, control, target, circuit, CU)

    return circuit, t, control, target
